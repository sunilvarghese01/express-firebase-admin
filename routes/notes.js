var express = require('express');
var router = express.Router();
var noteCtrl = require('../controllers/NotesController');


router.get('/', noteCtrl.get_all_notes);
router.post('/save', noteCtrl.add_note);
router.post('/update/:id', noteCtrl.update_note);
router.get('/delete/:id', noteCtrl.delete_note);
router.get('/new', function(req, res, next) {
  res.render('notes/form',{ page: req.url,action: '/notes/save' ,note: {}, page_title: 'Create new note' });
});

router.get('/edit/:id', noteCtrl.edit_note);
module.exports = router;
