# Sample Firebase Node App

This simple [Express](https://expressjs.com/) app is an example with firebase admin 

# Description 


This app is based on crud operation it's create new notes



Clone this repository:

$ git clone https://sunilvarghese01@bitbucket.org/sunilvarghese01/express-firebase-admin.git
$ cd express-firebase-admin


### Setup for First-time Users

node -v 8.10.0


if you are using nvm 
use -- nvm use 8.10.0 


npm install

###  start server

npm start


### url 

http://localhost:3000/notes

