var firebase = require("firebase-admin");
var db = firebase.database();
var ref = db.ref("restricted_access/secret_document");

NoteProvider = function() {
  this.fetchAllNotes = function(cb) {
		ref.once("value", function(snapshot) {
			let notes = snapshot.val().notes
		  cb(notes);
		},function (errorObject) {
  		console.log("The read failed: " + errorObject.code);
		})
  };

  this.fetchNote = function(id,cb) {
  	console.log(id)
		ref.once("value", function(snapshot) {
			let notes = snapshot.val().notes
		  cb(notes[id]);
		},function (errorObject) {
		  console.log("The read failed: " + errorObject.code);
		});
  };

  this.addNote = function(note, cb) {
		ref.once("value", function(snapshot) {
		  console.log(snapshot.val());
		});
		var noteRef = ref.child("notes");
		var newNoteRef = noteRef.push();
		newNoteRef.set({
			title: note.title,
			description: note.description
		})
    cb("successFully Saved", note);
  };
  

  this.updateNote = function(note,id,cb) {
		ref.once("value", function(snapshot) {
		  //console.log(snapshot.val());
		});
		var noteRef = ref.child("notes");
		var objRef = noteRef.child(id)
		objRef.update({
			title: note.title,
			description: note.description
		})
    cb("successFully Saved", note);
  };

  this.deleteNote = function(id,cb) {
		var noteRef = ref.child("notes");
		console.log(id)
		var objRef = noteRef.child(id)
		objRef.remove()
		cb("successFully removed");
  };
};

exports.NoteProvider = NoteProvider;