let _ = require('lodash')

var NoteProvider = require('../services/NoteProvider').NoteProvider;
var noteProvider  = new NoteProvider();

exports.get_all_notes = (req, res, next) => {
  noteProvider.fetchAllNotes(function(notes){
      res.render('notes/notes', { title: 'List of notes', notes: notes });
  })
}


exports.edit_note = (req, res,next) => {
   let id = req.params.id
   noteProvider.fetchNote(req.params.id,function(note){
      res.render('notes/form', {  page: req.url,action: '/notes/update/'+id ,note: note, page_title: 'Edit  note'});
  })
}

exports.add_note = (req, res, next) => {
 noteProvider.addNote(req.body, function(message,note){
    req.flash('success_messages', message);
 	  res.redirect('/notes');
 });
}

exports.update_note = (req, res, next) => {
  let id = req.params.id
 noteProvider.updateNote(req.body,id,function(message,note){
    req.flash('success_messages', message);
    res.redirect('/notes');
 });
}

exports.delete_note = (req, res, next) => {
  let id = req.params.id
  noteProvider.deleteNote(id,function(message){
    req.flash('success_messages', message);
    res.redirect('/notes');
 });
}
